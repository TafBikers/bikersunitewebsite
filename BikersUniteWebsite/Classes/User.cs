﻿using BikersUniteWebsite.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace BikersUniteWebsite.Classes
{
    public class User
    {
        //Calls api to insert a user upon registration
        public object RegisterUser(UserModel userDetails)
        {
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "UserRegister/PostUserRegistration");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(userDetails);

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);
          
            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }

        //Calls api to log a user in
        public object LoginUser(LoginModel loginDetails)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "Login");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(loginDetails);

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);

            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }

        //Calls api to retrieve a user profile
        public object GetUserProfile(SingleUserIdModel userid)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "UserRegister/GetUserProfile");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(userid);

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);

            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }

        //Calls api to send edited user data back to database
        public object EditUserProfile(UserModel userDetails)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "UserRegister/PutUserRegistration");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(userDetails);

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);

            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }
    }
}
