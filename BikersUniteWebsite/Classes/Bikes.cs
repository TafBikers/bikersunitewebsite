﻿using BikersUniteWebsite.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace BikersUniteWebsite.Classes
{
    public class Bikes
    {
        //Calls api to insert a new bike
        public object AddBike(BikeModel bikedetails)
        {
            
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "BikeDetail/AddNewBike");
            request.Method = "POST";
            
            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(bikedetails);
            
            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);
          
            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }

        //Calls api to retrieve bike details
        public object GetBikeDetails(SingleUserIdModel userId)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "BikeDetail/GetBikeList");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(userId);

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);

            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }

        //Calls api to send updated bike details
        public object EditBikeDetails(BikeModel bikedetails)
        {

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "BikeDetail/UpdateBike");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            var serializer = new JavaScriptSerializer();
            string jsonContent = serializer.Serialize(bikedetails);

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent);

            request.ContentLength = byteArray.Length;
            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);

            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Server could not be reached" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }
    }
}
