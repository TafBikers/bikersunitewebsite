﻿using BikersUniteWebsite.Models;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace BikersUniteWebsite.Classes
{
    public class EmailSubmit
    {
        private EmailResponse response = new EmailResponse();
        //Sets the databaseProviderFactory
        static EmailSubmit()
        {
            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
        }

        //Inserts a email address to a text file for storage
        public string AddEmailDetails(string name, string email)
        { 
            try
                {
            DbCommand dbCommand;
            //DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");

            string sqlCommand = "USP_Ins_NameEmail";
            dbCommand = db.GetStoredProcCommand(sqlCommand);
            db.DiscoverParameters(dbCommand);
            dbCommand.Parameters["@Firstname"].Value = UseRegex(name);
            dbCommand.Parameters["@Email"].Value = UseRegex(email);
            dbCommand.Parameters["@Message"].Direction = ParameterDirection.Output;
            DataSet ds = db.ExecuteDataSet(dbCommand);
            string msg = dbCommand.Parameters["@Message"].Value.ToString();
            return "Success" + msg;
            }
            catch (Exception ex)
            {
                //response.Status = "Error";
                //response.Status = "Could not connect to database" + " " + ex.Message;
                return "Error";
            }
        }

        //Cleans input string
        public static string UseRegex(string strIn)
        {
            // Replace invalid characters with empty strings.
            return Regex.Replace(strIn, @"[^\w\.@-]", "");
        }

        //Sends email from contact form to specific user
        public string SendEmail(string name, string email, string number, string msgbody)
        {
            try
            {
                string MySubject = "Message from Bikers Unite Website";
                string MyMessageBody = "You have a request from" + name + " " + number + " " + msgbody;
                string RecipientEmail = email;

                //Config settings
                string FromEmail = ConfigurationManager.AppSettings["FromEmail"];
                string DisplayName = ConfigurationManager.AppSettings["DisplayName"];
                string FromEmailPassword = ConfigurationManager.AppSettings["EmailPassword"];
                string HOST = ConfigurationManager.AppSettings["Host"];
                string Port = ConfigurationManager.AppSettings["Port"]; //TLS Port

                //Build email
                MailMessage mail = new MailMessage();
                mail.Subject = MySubject;
                mail.Body = MyMessageBody;
                mail.To.Add(RecipientEmail);
                mail.From = new MailAddress(FromEmail, DisplayName);

                SmtpClient SMTP = new SmtpClient(HOST);
                SMTP.EnableSsl = true;
                SMTP.Credentials = new System.Net.NetworkCredential(FromEmail.Trim(), FromEmailPassword.Trim());
                SMTP.DeliveryMethod = SmtpDeliveryMethod.Network; 
                SMTP.Port = Convert.ToInt32(Port);
                SMTP.Send(mail);
                response.Status = "Success";
                response.Message = "Sent Message To : " + RecipientEmail + " " + "Sent!";
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }
    }
}