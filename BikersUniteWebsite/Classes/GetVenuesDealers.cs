﻿using BikersUniteWebsite.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace BikersUniteWebsite.Classes
{
    public class GetVenuesDealers
    {
        //Calls api to retrieve all venues and dealers
        public object Post([FromBody]string value)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "Venue/GetAllVenueDealers");
            request.Method = "POST";

            request.UseDefaultCredentials = true;
            request.PreAuthenticate = true;
            request.Credentials = CredentialCache.DefaultCredentials;

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();

            request.ContentType = @"application/json";
            request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);

            try
            {
                using (Stream dataStream = request.GetRequestStream())
                {
                    HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                    if (response.StatusCode.ToString() == "OK")
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            StreamReader reader = new StreamReader(responseStream, encoding);

                            var serializer = new JavaScriptSerializer();
                            object jsonObject = serializer.DeserializeObject(reader.ReadToEnd());
                            return jsonObject;
                        }
                    }
                    else
                    {
                        string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
                        return "Venues and dealers are unavailable" + " " + Statuses;
                    }
                }
            }
            catch (WebException e)
            {
                return "Could not connect to service" + "" + e.Message;
            }
        }
    }
}

//Commented code for reference purposes
//public string Post([FromBody]string value)
//{
//    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["APIBaseUrl"] + "Venue/GetAllVenueDealers");
//    request.Method = "POST";

//    request.UseDefaultCredentials = true;
//    request.PreAuthenticate = true;
//    request.Credentials = CredentialCache.DefaultCredentials;

//    //string jsonContent = "{\"userID\":\"152\",\"latitude\":\"28.62334\",\"longitude\":\"77.38020\",\"searchKeyword\":\"\",\"pageNo\":\"1\",\"categoryId\":\"5\",\"featured\":0,\"isLongRun\":0,\"isDealer\":0}";
//    // "{\"userID\":\"137\",\"latitude\":\"-26.107447\",\"longitude\":\"28.05691\",\"searchKeyword\":\"\",\"pageNo\":\"1\",\"categoryId\":\"5\",\"featured\":0,\"isLongRun\":0,\"isDealer\":0}";

//    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
//    //Byte[] byteArray = encoding.GetBytes(jsonContent);

//    //request.ContentLength = byteArray.Length;
//    request.ContentType = @"application/json";
//    request.Headers.Add(HttpRequestHeader.Authorization, ConfigurationManager.AppSettings["APiAccessToken"]);
//    JObject jObj = new JObject();
//    string jsonObject;
//    using (Stream dataStream = request.GetRequestStream())
//        //{
//        //    dataStream.Write(byteArray, 0, byteArray.Length);
//        //}
//        try
//        {
//            HttpWebResponse response = request.GetResponse() as HttpWebResponse;
//            if (response.StatusCode.ToString() == "OK")
//            {
//                using (Stream responseStream = response.GetResponseStream())
//                {
//                    StreamReader reader = new StreamReader(responseStream, encoding);
//                    //jObj = reader.ReadToEnd();
//                    //var javaScriptSerializer = new JavaScriptSerializer();
//                    jsonObject = reader.ReadToEnd();
//                    //jObj = JObject.Parse(jsonObject);
//                    //string jstring = Convert.ToString(o);
//                    //VenueDealerDetails venuedealerDetail = new VenueDealerDetails();
//                    string jsonString = javaScriptSerializer.Deserialize(jsonObject);
//                    //var list = new JavaScriptSerializer().Deserialize<VenueDealerDetails>(jsonObject);

//                    //var Data = javaScriptSerializer.Deserialize<List<VenueDealerDetails>(jsonObject);
//                    //var obj = JSON.parse();
//                    //var serializer = new JavaScriptSerializer();
//                    //jsonObject = reader.ReadToEnd();
//                    //string jsonObject = serializer.DeserializeObject(reader.ReadToEnd()).ToString();
//                    //Can be returned in a few different ways have a look what you would like
//                    //object json = JsonConvert.SerializeObject(reader.ReadToEnd());
//                    //object json = reader.ReadToEnd();

//                    return jObj;
//                }
//            }
//            else
//            {
//                string Statuses = response.StatusCode.ToString() + " " + response.StatusDescription;
//                return "Venues and dealers are unavailable" + " " + Statuses;
//            }
//        }
//        catch (WebException e)
//        {
//            return "error";
//        }
//}
