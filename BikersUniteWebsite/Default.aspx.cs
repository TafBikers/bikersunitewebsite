﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using BikersUniteWebsite.Classes;
using BikersUniteWebsite.Models;
using Newtonsoft.Json.Linq;

namespace BikersUniteWebsite
{
    [ScriptService]
    public partial class _Default : Page
    {
        #region Venues
        //[ScriptMethod, WebMethod]
        //Webmethod to be called from frontend to send back all venues and dealers
        [ScriptMethod(ResponseFormat = ResponseFormat.Json), WebMethod]
        public static object GetVenues()
        {
            GetVenuesDealers VenuesDealers = new GetVenuesDealers();
            object venues = VenuesDealers.Post("");
            return venues;
        }
        #endregion

        #region Email
        //Inserts a user email address for signup purposes
        [ScriptMethod, WebMethod]
        public static string InsertEmail(string name, string email)
        {
            EmailSubmit AddEmail = new EmailSubmit();
            string msg = AddEmail.AddEmailDetails(name, email);
            return msg;
        }

        //Sends email when user fills out a contact form
        [ScriptMethod, WebMethod]
        public static string SendMail(string name, string email, string number, string msgbody)
        {
            EmailSubmit sendemail = new EmailSubmit();
            string msg = sendemail.SendEmail(name, email, number, msgbody);
            return msg;
        }
        #endregion

        #region User
        //Inserts a new user upon registration
        [ScriptMethod, WebMethod]
        public static object RegisterUser(UserModel userdetails)
        {
            User newUser = new User();
            object newUserDetails = newUser.RegisterUser(userdetails);
            return newUserDetails;
        }

        //When proper credentials are given user willl be logged in
        [ScriptMethod, WebMethod]
        public static object LoginInfo(LoginModel loginDetails)
        {
            User newLogin = new User();
            object newUserDetails = newLogin.LoginUser(loginDetails);
            return newUserDetails;
        }

        //Retrieves the complete user profile for view or edit
        [ScriptMethod, WebMethod]
        public static object GetUserProfile(SingleUserIdModel userId)
        {
            User getProfile = new User();
            object ProfileDetails = getProfile.GetUserProfile(userId);
            return ProfileDetails;
        }

        //Edits a user profile
        [ScriptMethod, WebMethod]
        public static object EditUserProfile(UserModel userdetails)
        {
            User newUser = new User();
            object newUserDetails = newUser.EditUserProfile(userdetails);
            return newUserDetails;
        }
        #endregion

        #region Bike
        //Currently busy with
        //Adds a bike with a link to the user in the database
        [ScriptMethod, WebMethod]
        public static object AddBike(BikeModel bikedetails)
        {
            Bikes newBike = new Bikes();
            object newBikeDetails = newBike.AddBike(bikedetails);
            return newBikeDetails;
        }
        
        //Retrieves bike info for view or editing purposes
        [ScriptMethod, WebMethod]
        public static object GetBikeInfo(SingleUserIdModel userId)
        {
            Bikes getBike = new Bikes();
            object bikes = getBike.GetBikeDetails(userId);
            return bikes;
        }
       
        //Sends updated information for bikes to the api
        [ScriptMethod, WebMethod]
        public static object EditBikeInfo(BikeModel bikedetails)
        {
            Bikes updateBike = new Bikes();
            object bikes = updateBike.EditBikeDetails(bikedetails);
            return bikes;
        }
        #endregion

        #region Crash

        [ScriptMethod, WebMethod]
        public static object GetCrashInfo(SingleUserIdModel userId)
        {
            CrashInformation crash = new CrashInformation();
            object crashinfo = crash.GetCrashInfo(userId);
            return crashinfo;
        }
        [ScriptMethod, WebMethod]
        public static object EditCrashInfo(CrashInfoModel crashdetails)
        {
            CrashInformation crash = new CrashInformation();
            object crashinfo = crash.AddEditCrashInfo(crashdetails);
            return crashinfo;
        }

        #endregion
    }
}