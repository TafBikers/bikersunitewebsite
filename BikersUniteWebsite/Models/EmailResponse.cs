﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace BikersUniteWebsite.Models
{
    public class EmailResponse
    {
        //Email response properties
        public string Status { get; set; }
        public string Message { get; set; }
    }
}