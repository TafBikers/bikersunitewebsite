﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using System.Web;

namespace BikersUniteWebsite.Models
{
    public class CrashInfoModel
    {
        //Bike object properties
        public string userId { get; set; }
        public string primaryName { get; set; }
        public string primaryPhoneNo { get; set; }
        public string secondaryName { get; set; }
        public string secondaryPhoneNo { get; set; }
        public string MedicalAidName { get; set; }
        public string medicalAidNumber { get; set; }
        public string riderCellNo { get; set; }
        public int PassengerOnBoard { get; set; }
        public int NotifyEmergencyService { get; set; }
        public int CrashdetectionId { get; set; }

    }
}
