﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BikersUniteWebsite.Models
{
    public class SingleUserIdModel
    {
        //single propertie to retrieve multiple items needing just userid
        public int UserId { get; set; }
    }
}
