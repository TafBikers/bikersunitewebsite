﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BikersUniteWebsite.Models
{
    public class UserModel
    {
        //user object properties
        public int userId { get; set; }
        public string name { get; set; }
        public string emailAddress { get; set; }
        public string gender { get; set; }
        public string dob { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public bool isActive { get; set; }
        public bool optIn { get; set; }
        public string userPhoto { get; set; }
        public string description { get; set; }
        public bool facebook { get; set; }
        public bool twitter { get; set; }
        public bool google { get; set; }
        public int placesVisited { get; set; }
        public int reviewSubmitted { get; set; }
        public bool isPremium { get; set; }
        public string deviceToken { get; set; }
        public string platform { get; set; }
        public string unit { get; set; }

    }
}
