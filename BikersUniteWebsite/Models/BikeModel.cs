﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.InteropServices;
using System.Web;

namespace BikersUniteWebsite.Models
{
    public class BikeModel
    {
        //Bike object properties
        public int bikeDetailsId { get; set; }
        public string userId { get; set; }
        public string year { get; set; }
        public string millage { get; set; }
        public string bikeMakeID { get; set; }
        public string bikeModelId { get; set; }
        public string licExpiry { get; set; }
        public string nextService { get; set; }
        public string bikeLogo { get; set; }
        public string lastService { get; set; }
        public string lastTyrePurchase { get; set; }
        public string makerImage { get; set; }
        public string imageExtension { get; set; }
    }
}
