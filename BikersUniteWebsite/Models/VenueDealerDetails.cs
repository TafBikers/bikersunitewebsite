﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BikersUniteWebsite.Models
{
    public class VenueDealerDetails
    {
        //venue and dealer object properties
        public int VenueDetailsId { get; set; }
        public string VenueName { get; set; }
        public int? UserId { get; set; }
        public string Location { get; set; }
        public string VenueDescription { get; set; }
        public string FacebookLink { get; set; }
        public string WebsiteLink { get; set; }
        public string TwitterLink { get; set; }
        public string InstagramLink { get; set; }
        public string MapsLocation { get; set; }
        public string ProfileImage { get; set; }
        public string CoverImage { get; set; }
        public int VenueId { get; set; }
        public decimal? Distance { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public int CategoryId { get; set; }
        public bool Featured { get; set; }
        public long? TotalReviewCount { get; set; }
    }
}
