﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="BikersUniteWebsite._Default" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   
    <div class="jumbotron">
        <h1>Bikers Unite</h1>
        <p class="lead"></p>
    </div>

    <div class="row">
        <div class="col-md-4">
           
        </div>
        <div class="col-md-4">
          
            
            <table style="width: 46%;">
                <tr>
                    <td>
                        <asp:Label ID="lblName" runat="server" Text="Name"></asp:Label>
                        <asp:TextBox ID="txtName" runat="server" AutoCompleteType="FirstName"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" Text="Email"></asp:Label>
                        <asp:TextBox ID="txtEmail" runat="server" AutoCompleteType="Email" CausesValidation="False"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="test" runat="server" Text="Button" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
           
        </div>
    </div>

</asp:Content>
